package exercise;

class App {
    // BEGIN
    public static String getTypeOfTriangle(int a, int b, int c){
        if ((a + b) <= c){
            return "Треугольник не существует";
        } else if ((a + c) <= b) {
            return "Треугольник не существует";
        } else if ((b + c) <= a) {
            return "Треугольник не существует";
        } else {
            if (a == b && b == c){
                return "Равносторонний";
            } else if (a == b || b == c || a == c){
                return "Равнобедренный";
            }else {
                return "Разносторонний";
            }
        }
    }
    // END

    // BEGIN IW
    public static int getFinalGrade(int exam, int project){
        if (exam > 90 && project > 10){
            return 100;
        } else if (exam > 75 && project >= 5) {
            return 90;
        } else if (exam > 50 && project >= 2) {
            return 75;
        } else {
            return 0;
        }
    }
    // END IW
}
