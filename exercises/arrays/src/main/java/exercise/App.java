package exercise;

class App {
    // BEGIN
    public static int[] reverse(int[] numbers) {
        int l = numbers.length;
        int[] result = new int[l];
        for (int i = 0; i < numbers.length; i++) {
            l--;
            result[i] = numbers[l];
        }
        return result;
    }

    public static int mult(int[] numbers){
        int result = 1;
        for (int i = 0; i < numbers.length; i++){
            result = result * numbers[i];
        }
        return result;
    }
    // END
}
