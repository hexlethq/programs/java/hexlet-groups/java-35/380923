package exercise;

import java.util.List;
import java.util.Map;

// BEGIN
class Sorter {
    public static List<String> takeOldestMans(List<Map<String, String>> users) {
        return users.stream()
                .filter(user -> user.get("gender").equals("male"))
                .sorted((user1, user2) -> {
                    String birthday1 = user1.get("birthday");
                    String birthday2 = user2.get("birthday");
                    return birthday1.compareTo(birthday2);
                })
                .map(user -> user.get("name"))
                .toList();
    }
}
// END
