package exercise;

import java.util.Arrays;
import java.util.stream.Stream;

// BEGIN
class App {
    public static String[][] enlargeArrayImage(String[][] symbols) {
        return Arrays.stream(symbols)
                .flatMap(row -> {
                    String[] scaledRow = Arrays.stream(row)
                            .flatMap(el -> Stream.of(el, el))
                            .toArray(String[]::new);
                    return Stream.of(scaledRow, scaledRow);
                }).toArray(String[][]::new);
    }
//  public static String[][] enlargeArrayImage(String[][] symbols) {
//          return Arrays.stream(symbols)
//                  .map(arraysArray ->
//                      Arrays.stream(arraysArray)
//                              .flatMap(rowArray  -> Stream.of(rowArray, rowArray))
//                              .toArray((el) -> new String[el]))
//                  .flatMap(arraysArray  -> Stream.of(arraysArray, arraysArray))
//                  .toArray(String[][]::new); // .toArray(x -> new String[x][]);
//    }

}
// END
