package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static int getIndexOfMaxNegative(int[] arr) {
        int maxMin = Integer.MIN_VALUE;
        int index = -1;
        for(int i = 0; i < arr.length; i++) {
            if (maxMin < arr[i] && arr[i] < 0) {
                maxMin = arr[i];
                index = i;
            }
        }
     return index;
    }

    public static int[] getElementsLessAverage(int[] arr1) {
        int[] getELA = new int[arr1.length];
        int result = 0;
        for(int a: arr1) {
            result += a;
        }
        if (result == 0){
            return getELA;
        }
        int max = result / arr1.length;
        int i1 = 0;
        for(int i = i1 = 0; i < arr1.length; i++) {
            if (arr1[i] <= max){
                System.arraycopy(arr1, i, getELA, i1, 1);
                i1++;
            }
        }
        return Arrays.copyOf(getELA, i1);
    }

    public static int getSumBeforeMinAndMax(int[] arr) {
        int result = 0;
        int max = arr[0];
        int min = arr[0];

        for (int i = 1; i < arr.length; i++) {
            min = min > arr[i] ? arr[i] : min;
        }
        for (int i = 1; i < arr.length; i++) {
            max = max < arr[i] ? arr[i] : max;
        }

        int imax = 0;
        int imi = 0;

        for (int i = 1; i < arr.length; i++) {
           imax = max == arr[i] ? i : imax;
        }
        for (int i = 1; i < arr.length; i++) {
            imi = min == arr[i] ? i : imi;
        }

        if (imax > imi){
            for (int i = imi + 1; i < imax; i++) {
                result += arr[i];
            }
        }
        for (int i = imax + 1; i < imi; i++) {
            result += arr[i];
        }
        return result;
    }
    // END
}

