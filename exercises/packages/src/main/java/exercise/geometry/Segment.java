// BEGIN
package exercise.geometry;

import java.util.Arrays;

public class Segment {
    public static double[][] segment = new double[2][2];

    public static double[][] makeSegment(double[] point1, double[] point2) {
        System.arraycopy(point1, 0, Segment.segment[0], 0, 2);
        System.arraycopy(point2, 0, Segment.segment[1], 0, 2);
        return Segment.segment;
    }

    public static double[] getBeginPoint(double[][] segment) {
        double[] beginPoint = Arrays.copyOf(segment[0], 2);
        return beginPoint;
    }

    public static double[] getEndPoint(double[][] segment) {
        double[] endPoint = Arrays.copyOf(segment[1], 2);
        return endPoint;
    }

}
// END
