package exercise;

// BEGIN
class App {

    public static double[] getMidpointOfSegment(double[][] segment) {
        double x = (segment[0][0] + segment[1][0]) / 2;
        double y = (segment[0][1] + segment[1][1]) / 2;
        double[] result = {x, y};
        return result;
    }

    public static double[][] reverse(double[][] point) {
        double[][] reservePoint = new double[2][2];
        System.arraycopy(point[1], 0, reservePoint[0], 0, 2);
        System.arraycopy(point[0], 0, reservePoint[1], 0, 2);
        return reservePoint;
    }

}
// END
