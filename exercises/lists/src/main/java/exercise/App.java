package exercise;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

// BEGIN
class App {
    public static boolean scrabble(String symbolSet, String world) {

        if (symbolSet.length() == 0 || world.length() == 0) {
            return false;
        }

        List<Character> listSymbolSet = new ArrayList<>();
        List<Character> listWorld = new ArrayList<>();

        for (int i = 0; i < symbolSet.length(); i++) {
            listSymbolSet.add(symbolSet.charAt(i));
        }

        for (int i = 0; i < world.length(); i++) {
            listWorld.add(world.toLowerCase().charAt(i));
        }

        for (Character symbol: listWorld) {
            if (!listSymbolSet.remove(symbol)) {
                return false;
            }
        }
        return true;
    }
}
//END
