package exercise;

import java.util.List;

// BEGIN
class App {

    public static String getForwardedVariables(String config) {
        String varPrefix = "X_FORWARDED_";
        String varDelimiter = ",";
        List<String> lines = List.of(config.split("\n"));
        List<String> variables = lines.stream()
                .filter(x -> x.startsWith("environment") && x.indexOf(varPrefix) > -1)
                .map(envString -> {
                    int quoteIndex = envString.indexOf("\"");
                    String variablesString = envString.substring(quoteIndex + 1, envString.length() - 1);
                    List<String> keyValues = List.of(variablesString.split(varDelimiter)).stream()
                            .filter(keyValue -> keyValue.startsWith(varPrefix))
                            .map(keyValue -> keyValue.replace(varPrefix, ""))
                            .toList();
                    return String.join(varDelimiter, keyValues);
                }).toList();
        return String.join(varDelimiter, variables);
    }


}
//END
