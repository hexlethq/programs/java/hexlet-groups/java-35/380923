package exercise;

class App {
    // BEGIN
    public static int[] sort(int[] arr) {
        for (int i = 1; i < arr.length; i++) {
            for (int k = i; k > 0; k--) {
                if (arr[k] < arr[k - 1]) {
                    arr[k] += arr[k - 1];
                    arr[k - 1] = arr[k] - arr[k - 1];
                    arr[k] = arr[k] - arr[k - 1];
                }
            }
        }
        return arr;
    }
    // END
}
