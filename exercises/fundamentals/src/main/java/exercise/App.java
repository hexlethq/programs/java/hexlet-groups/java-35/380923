package exercise;

class App {

    public static void main(String[] args) {
        numbers();
        strings();
        converting();
    }

    public static void numbers() {
        // BEGIN
        System.out.println((8 / 2) + (10 % 3));
        // END
    }

    public static void strings() {
        String language = "Java";
        // BEGIN
        String strWorks = "works";
        String strOn = "on";
        String strJVM= "JVM";
        String strSp = " ";
        String res = language + strSp + strWorks + strSp + strOn + strSp + strJVM;
        System.out.println(res);
        // END
    }

    public static void converting() {
        Number soldiersCount = 300;
        String name = "spartans";
        // BEGIN
        String res = soldiersCount + " " + name;
        System.out.println(res);
        // END
    }
}
