package exercise;

import java.util.Arrays;


// BEGIN
class Kennel {
    public static String[][] puppies = new String[0][2];

    public static void addPuppy(String[] puppy) {
        Kennel.puppies = Arrays.copyOf(Kennel.puppies, Kennel.puppies.length + 1);
        Kennel.puppies[Kennel.puppies.length - 1] = puppy;
    }

    public static void addSomePuppies(String[][] puppies) {
        int length = Kennel.puppies.length;
        Kennel.puppies = Arrays.copyOf(Kennel.puppies,  Kennel.puppies.length + puppies.length);
        for (int i = 0; i < puppies.length; i++) {
            Kennel.puppies[length + i] = puppies[i];
        }
    }

    public static int getPuppyCount() {
        return Kennel.puppies.length;
    }

    public static boolean isContainPuppy(String name) {
        for (int i = 0; i < Kennel.puppies.length; i++) {
            if (Kennel.puppies[i][0].equals(name)) {
                return true;
            }
        }
        return false;
    }

    public static String[][] getAllPuppies() {
        return Kennel.puppies;
    }

    public static String[] getNamesByBreed(String breed) {
        String[] names = new String[0];
        for (int i = 0; i < Kennel.puppies.length; i++) {
            if (breed.equals(Kennel.puppies[i][1])) {
                names = Arrays.copyOf(names, names.length + 1);
                names[names.length - 1] = Kennel.puppies[i][0];
            }
        }
        return names;
    }

    public static void resetKennel() {
        Kennel.puppies = Arrays.copyOf(Kennel.puppies, 0);
    }
}
// END
