package exercise;

class Converter {
    // BEGIN
    public static int convert(int number, String direction) {
        if (direction == "b") {
            return number * 1024;
        } else if (direction == "Kb") {
            return number / 1024;
        } else {
            return 0;
        }
    }

    public static void main(String[] args) {
        int res = convert(10, "b");
        System.out.println("10 Kb = " + res + " b");
    }
    // END
}
