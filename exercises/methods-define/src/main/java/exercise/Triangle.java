package exercise;

class Triangle {
    // BEGIN
    public static double getSquare(int a, int b, int alphaGrad) {
        double alphaRad = (alphaGrad * Math.PI)/180;
        return ((a * b) / 2) * Math.sin(alphaRad);

    }

    public static void main(String[] args) {
        double res = getSquare(4, 5, 45);
        System.out.println(res);
    }
    // END
}
