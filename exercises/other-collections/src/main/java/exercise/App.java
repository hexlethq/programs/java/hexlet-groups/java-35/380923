package exercise;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

// BEGIN
class App {
    public static LinkedHashMap<String, String> genDiff(Map<String, Object> map1, Map<String, Object> map2) {
        Set<String> keys = new TreeSet();
        keys.addAll(map1.keySet());
        keys.addAll(map2.keySet());

        return keys.stream()
                .collect(Collectors.toMap(key -> key,
                        key -> {
                            if (!map2.containsKey(key)) {
                                return "deleted";
                            }
                            if (!map1.containsKey(key)) {
                                return "added";
                            }
                            return map1.get(key).equals(map2.get(key)) ? "unchanged" : "changed";
                        },
                        (key1, key2) -> key1,
                        LinkedHashMap::new));

    }
}
//END
