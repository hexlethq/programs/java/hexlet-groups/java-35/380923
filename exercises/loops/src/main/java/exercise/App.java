package exercise;

class App {
    // BEGIN
    public static String getAbbreviation(String word) {
        String result = "" + word.charAt(0);

        for (int i = 1; i < word.length(); i++){
            if ((' ' == word.charAt(i - 1)) && (word.charAt(i) != ' ')) {
                result = result + word.charAt(i);

            }
        }

        return (result.toUpperCase()).trim();
    }
    // END
}