package exercise;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

// BEGIN
public class App {
    public static List findWhere(List<Map<String, String>> books, Map<String, String> searchingBook) {

        List<Map> result = new ArrayList<>();
        for (Map<String, String> currentBook : books) {
            if (currentBook.entrySet().containsAll(searchingBook.entrySet())) {
                result.add(currentBook);
            }
        }
        return result;
    }
}
//END
