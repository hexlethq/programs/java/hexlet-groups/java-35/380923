package exercise;

import java.time.LocalDate;
import java.util.Arrays;

class App {
    // BEGIN
    public static String buildList(String[] arr) {
        StringBuilder result = new StringBuilder();
        String res = "";
        if (arr.length == 0) {
            return res;
        }

        result.append("<ul>");
        for (int i = 0; i < arr.length; i++) {
            result.append("\n  <li>" + arr[i] + "</li>");
        }
        result.append("\n</ul>");
        return result.toString();
    }

    public static String getUsersByYear(String[][] usersList, int date1) {
        String[] names = new String[0];

        for (int i = 0; i < usersList.length; i++) {
            LocalDate date2 = LocalDate.parse(usersList[i][1]); // создаем объект LocalDate на основе анализа текста в формате даты
            if (date1 == date2.getYear()) {
                names = Arrays.copyOf(names, names.length + 1);
                names[names.length - 1] = usersList[i][0];
            }
        }
        String result = buildList(names);
        return result;
    }

  }
//    // END
//
//    // Это дополнительная задача, которая выполняется по желанию.
////    public static String getYoungestUser(String[][] users, String date) throws Exception {
//        // BEGIN
//        
//        // END
//    }
//}
