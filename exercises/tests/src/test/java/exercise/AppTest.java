package exercise;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class AppTest {

    @Test
    void testTake() {
        // BEGIN
        List<Integer> numbers1 = new ArrayList<>(Arrays.asList(1, 2, 3, 4));
        List<Integer> numbers1Expected = new ArrayList<>(Arrays.asList(1, 2));
        int number1 = 2;
        Assertions.assertThat(App.take(numbers1, number1))
                .containsAll(numbers1Expected);

        List<Integer> numbers2 = new ArrayList<>(Arrays.asList(7, 3, 10));
        List<Integer> numbers2Expected = new ArrayList<>(Arrays.asList(7, 3, 10));
        int number2 = 8;
        Assertions.assertThat(App.take(numbers2, number2))
                .containsAll(numbers2Expected);

        List<Integer> numbers3 = new ArrayList<>(Arrays.asList(2, 4, 6, 8, 10, 12));
        List<Integer> numbers3Expected = new ArrayList<>();
        int number3 = 0;
        Assertions.assertThat(App.take(numbers3, number3))
                .containsAll(numbers3Expected);
        // END
    }
}
