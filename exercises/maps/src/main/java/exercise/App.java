package exercise;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// BEGIN
class App {
    public static HashMap getWordCount(String word) {
        Map<String, Integer> words = new HashMap<>();
        String[] sentence = word.split(" ");
        if (word.isBlank()) {
            return (HashMap) words;
        }
        for (int i = 0; i < sentence.length; i++) {
            int n = words.getOrDefault(sentence[i], 0);
            words.put(sentence[i], n + 1);
        }
        return (HashMap) words;
    }

    public static String toString(Map<String, Integer> words) {
        if (words.size() == 0) {
            return "{}";
        }
        String result = "{\n";
        for (String s: words.keySet()) {
            result += ("  " + s + ": " + words.get(s) + "\n");
        }
        result += "}";
        return result;
    }
}
//END
