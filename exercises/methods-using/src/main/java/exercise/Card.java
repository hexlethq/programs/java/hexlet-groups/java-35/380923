package exercise;

class Card {
    public static void printHiddenCard(String cardNumber, int starsCount) {
// BEGIN
        String stern = "*";
        String formattedString = stern.repeat(starsCount) + cardNumber.subSequence(12, 16);
        System.out.println(formattedString);
// END
    }
}
