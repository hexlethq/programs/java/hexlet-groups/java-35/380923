package exercise;

import java.util.List;
import java.util.stream.Stream;

// BEGIN
class App {
    private static List<String> freeDomains = List.of("gmail.com", "yandex.ru", "hotmail.com");

    public static long getCountOfFreeEmails(List<String> emails) {
        return emails.stream()
                .filter((email) -> {
                    String domain = email.substring(email.indexOf("@") + 1);
                    return freeDomains.contains(domain);
                })
                .count();
    }
}
// END
