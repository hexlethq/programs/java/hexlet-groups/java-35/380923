package exercise;

class Point {
    // BEGIN
    public static int[] makePoint(int x, int y) {
        int[] point = {x, y};
        return point;
    }

    public static int getX(int[] arr) {
        return arr[0];
    }

    public static int getY(int[] arr) {
        return arr[1];
    }

    public static String pointToString(int[] arr) {
        String stringArr = "(" + arr[0] + ", " + arr[1] + ")";
        return stringArr;
    }

    public static int getQuadrant(int[] arr) {
        int result = 0;
    if (arr[0] > 0 && arr[1] > 0) {
        result = 1;
        return result;
    }
    else if (arr[0] < 0 && arr[1] > 0) {
        result = 2;
        return result;
    }
    else if (arr[0] < 0 && arr[1] < 0) {
        result = 3;
        return result;
    }
    else if (arr[0] > 0 && arr[1] < 0) {
        result = 4;
        return result;
    }
    return result;
    }
    // END
}
